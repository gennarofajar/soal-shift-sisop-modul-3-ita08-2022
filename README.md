# soal-shift-sisop-modul-3-ITA08-2022

## Anggota Kelompok :
1. 5027201042 - Ilham Muhammad Sakti
2. 5027201061 - Gennaro Fajar Mennde - 
3. 5027201067 - Naufal Ramadhan

## Daftar Isi
- [Soal](#soal)
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)

## Soal
Soal dapat dilihat di
[sini](https://docs.google.com/document/d/1w3tYGgqIkHRFoqKJ9nKzwGLCbt30HM7S/edit)
(harus dengan email ITS).

## Soal 1
Source code dapat dilihat di [sini](Soal 1/soal1.c)

## Cara Pengerjaan
1. Defini library yang digunakan 

Dalam membuat program Bahasa C yang berjalan di latar belakang/background, pertama-tama yang diperlukan adalah melakukan import library yang digunakan :
```c
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
```
- `<stdio.h>` library untuk fungsi input-output (e.g. `printf()`, `sprintf()`).
- `<string.h>` library untuk melakukan manipulasi arrays of character (e.g. `*memset()`).
- `<pthread.h>` library untuk mendapatkan ID dari pemanggilan sebuah thread(e.g. `pthread_self()`).
- `<stdlib.h>` library untuk fungsi umum (e.g. `exit()`, `atoi()`).
- `<unistd.h>` library untuk melakukan system call kepada kernel linux (e.g. `fork()`).
- `<sys/types.h>` library tipe data khusus (e.g. `pid_t`).
- `<sys/wait.h>` library untuk melakukan wait (e.g. `wait()`).
- `<sys/stat.h>` library untuk melakukan pengembalian dari status waktu (e.g. `time_t()`)

2. Inisialisasi awal thread dan child process

Lakukan inisialisai untuk menampung thread dan membuat child proses.
```c
pthread_t tid[2];
pthread_t tidUnzip[2];
pthread_t textDecode[2];
pthread_t tidQ[18];
pthread_t tidM[18];
pthread_t newFold;
pthread_t move;
pthread_t tidZip;
pthread_t txt;
pthread_t last;
pid_t child;
pid_t child1, child2;
```

3. Pendefinisian alamat file dan tujuan direktori yang akan digunakan 

Langkah selanjutnya adalah pendefinisian alamat-alamat file yang nanti akan digunakan pada program.
```c
char *url[] = {"https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"};
char *fileName[] = {"/home/ilham/Documents/modul3/soal1:/quote.zip", "/home/ilham/Documents/modul3/soal1:/music.zip"};
char *dir[] = {"/home/ilham/Documents/modul3/soal1:/quote", "/home/ilham/Documents/modul3/soal1:/music"};
char *fileQuote[] = {"/home/ilham/Documents/modul3/soal1:/quote/q1.txt", "/home/ilham/Documents/modul3/soal1:/quote/q2.txt", "/home/ilham/Documents/modul3/soal1:/quote/q3.txt", "/home/ilham/Documents/modul3/soal1:/quote/q4.txt", "/home/ilham/Documents/modul3/soal1:/quote/q5.txt", "/home/ilham/Documents/modul3/soal1:/quote/q6.txt", "/home/ilham/Documents/modul3/soal1:/quote/q7.txt", "/home/ilham/Documents/modul3/soal1:/quote/q8.txt", "/home/ilham/Documents/modul3/soal1:/quote/q9.txt"};
char *fileMusic[] = {"/home/ilham/Documents/modul3/soal1:/music/m1.txt", "/home/ilham/Documents/modul3/soal1:/music/m2.txt", "/home/ilham/Documents/modul3/soal1:/music/m3.txt", "/home/ilham/Documents/modul3/soal1:/music/m4.txt", "/home/ilham/Documents/modul3/soal1:/music/m5.txt", "/home/ilham/Documents/modul3/soal1:/music/m6.txt", "/home/ilham/Documents/modul3/soal1:/music/m7.txt", "/home/ilham/Documents/modul3/soal1:/music/m8.txt", "/home/ilham/Documents/modul3/soal1:/music/m9.txt"};
char *decode[] = {"/home/ilham/Documents/modul3/soal1:/quote/quote.txt", "/home/ilham/Documents/modul3/soal1:/music/music.txt"};
char *newFolder = "hasil";
char *passZip = "mihinomenestilham";
```
4. Fungsi `main()`

Pada fungsi main, kita membuat thread menggunakan fungsi `pthread_create()` sekaligus dalam pemanggilannya dipanggil fungsi yang akan dieksekusi saat thread tersebut dijalankan. Kemudian, `pthread_join()` digunakan untuk membuat program utama menunggu thread yang join hingga target thread selesai dieksekusi, dengan fungsi ini program utama di-suspend hingga target thread selesai dieksekusi.

```c
int main(void)
{
    pthread_create(&(tid[0]),NULL,&downloadQuote,NULL); 
    pthread_create(&(tid[1]),NULL,&downloadMusic,NULL); 
    pthread_create(&(tidUnzip[0]),NULL,&unzipQuote,NULL); 
    pthread_create(&(tidUnzip[1]),NULL,&unzipMusic,NULL); 
    pthread_create(&(tidQ[0]),NULL,&decodeQ1,NULL); 
    pthread_create(&(tidQ[1]),NULL,&enterQ1,NULL); 
    pthread_create(&(tidQ[2]),NULL,&decodeQ2,NULL); 
    pthread_create(&(tidQ[3]),NULL,&enterQ2,NULL); 
    pthread_create(&(tidQ[4]),NULL,&decodeQ3,NULL); 
    pthread_create(&(tidQ[5]),NULL,&enterQ3,NULL); 
    pthread_create(&(tidQ[6]),NULL,&decodeQ4,NULL); 
    pthread_create(&(tidQ[7]),NULL,&enterQ4,NULL); 
    pthread_create(&(tidQ[8]),NULL,&decodeQ5,NULL); 
    pthread_create(&(tidQ[9]),NULL,&enterQ5,NULL); 
    pthread_create(&(tidQ[10]),NULL,&decodeQ6,NULL); 
    pthread_create(&(tidQ[11]),NULL,&enterQ6,NULL); 
    pthread_create(&(tidQ[12]),NULL,&decodeQ7,NULL); 
    pthread_create(&(tidQ[13]),NULL,&enterQ7,NULL); 
    pthread_create(&(tidQ[14]),NULL,&decodeQ8,NULL); 
    pthread_create(&(tidQ[15]),NULL,&enterQ8,NULL); 
    pthread_create(&(tidQ[16]),NULL,&decodeQ9,NULL); 
    pthread_create(&(tidM[0]),NULL,&decode1,NULL); 
    pthread_create(&(tidM[1]),NULL,&enter1,NULL); 
    pthread_create(&(tidM[2]),NULL,&decode2,NULL); 
    pthread_create(&(tidM[3]),NULL,&enter2,NULL); 
    pthread_create(&(tidM[4]),NULL,&decode3,NULL); 
    pthread_create(&(tidM[5]),NULL,&enter3,NULL); 
    pthread_create(&(tidM[6]),NULL,&decode4,NULL); 
    pthread_create(&(tidM[7]),NULL,&enter4,NULL); 
    pthread_create(&(tidM[8]),NULL,&decode5,NULL); 
    pthread_create(&(tidM[9]),NULL,&enter5,NULL); 
    pthread_create(&(tidM[10]),NULL,&decode6,NULL); 
    pthread_create(&(tidM[11]),NULL,&enter6,NULL); 
    pthread_create(&(tidM[12]),NULL,&decode7,NULL); 
    pthread_create(&(tidM[13]),NULL,&enter7,NULL); 
    pthread_create(&(tidM[14]),NULL,&decode8,NULL); 
    pthread_create(&(tidM[15]),NULL,&enter8,NULL); 
    pthread_create(&(tidM[16]),NULL,&decode9,NULL); 
    pthread_create(&(newFold),NULL,&newDirectory, NULL); 
    pthread_create(&(move),NULL,&moveFile, NULL); 
    pthread_create(&(tidZip),NULL,&zipFold, NULL); 
    pthread_create(&(txt),NULL,&noTxtAndUnzip, NULL); 
    pthread_create(&(last),NULL,&soal3e, NULL); 

 
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tidUnzip[0], NULL);
    pthread_join(tidUnzip[1], NULL);
    pthread_join(tidQ[0],NULL);
	pthread_join(tidQ[1],NULL);
    pthread_join(tidQ[2],NULL);
    pthread_join(tidQ[3],NULL);
    pthread_join(tidQ[4],NULL);
    pthread_join(tidQ[5],NULL);
    pthread_join(tidQ[6],NULL);
    pthread_join(tidQ[6],NULL);
    pthread_join(tidQ[7],NULL);
    pthread_join(tidQ[8],NULL);
    pthread_join(tidQ[9],NULL);
    pthread_join(tidQ[10],NULL);
    pthread_join(tidQ[11],NULL);
    pthread_join(tidQ[12],NULL);
    pthread_join(tidQ[13],NULL);
    pthread_join(tidQ[14],NULL);
    pthread_join(tidQ[15],NULL);
    pthread_join(tidQ[16],NULL);
    pthread_join(tidM[0],NULL);
	pthread_join(tidM[1],NULL);
    pthread_join(tidM[2],NULL);
    pthread_join(tidM[3],NULL);
    pthread_join(tidM[4],NULL);
    pthread_join(tidM[5],NULL);
    pthread_join(tidM[6],NULL);
    pthread_join(tidM[6],NULL);
    pthread_join(tidM[7],NULL);
    pthread_join(tidM[8],NULL);
    pthread_join(tidM[9],NULL);
    pthread_join(tidM[10],NULL);
    pthread_join(tidM[11],NULL);
    pthread_join(tidM[12],NULL);
    pthread_join(tidM[13],NULL);
    pthread_join(tidM[14],NULL);
    pthread_join(tidM[15],NULL);
    pthread_join(tidM[16],NULL);
    pthread_join(newFold, NULL);
    pthread_join(move, NULL);
    pthread_join(tidZip, NULL);
    pthread_join(txt, NULL);
    pthread_join(last, NULL);
 
    return 0;
}
```

5. Fungsi `downloadQuote()` dan `downloadMusic()`

Fungsi ini berfungsi untuk mengunduh file quote.zip dan music.zip yang ada pada google drive praktikum sisop 2022 modul 3. Selain itu, mengubah nilai controller menjadi 2 pada fungsi `downloadQuote` dan mengubah nilai controller menjadi 3 pada fungsi `downloadMusic()`. `pthread_exit()` digunakan untuk keluar thread apabila prosesnya telah selesai.

```c
void* downloadQuote(void *arg)
{   
    char *argv1[] = {"wget", "-q", url[0], "-O", fileName[0], NULL};
    controller = 1;
    child = fork();
	if (child==0) {
        
        printf("1 - Download Quote\n");
	    execv("/usr/bin/wget", argv1);
	}

    pthread_exit(NULL);
}

void* downloadMusic(void *arg)
{   
    char *argv1[] = {"wget", "-q", url[1], "-O", fileName[1], NULL};
    
    child = fork();
    controller = 2;
	if (child==0) {
        printf("2 - Download Music\n");
	    execv("/usr/bin/wget", argv1);
	}

    pthread_exit(NULL);
}
```

6. Fungsi `unzipQuote()` dan `unzipMusic()`

Fungsi ini berfungsi untuk melakukan unzip pada file quote.zip dan music.zip. Kedua fungsi ini akan dijalankan apabila nilai controller adalah 2. Kemudian, memanggil `fork()` untuk memanggil *child process* dan mengubah nilai varibel controller menjadi 3 pada fungsi `unzipQuote()` serta menjadi 4 pada fungsi `unzipMusic()`. Pada *child process* dilakukan proses unzip file quote.zip dan music.zip.

```c
void* unzipQuote(void *arg)
{   
    while(1){
        if(controller == 2)
            break;
    }
    sleep(2);
    char *argv1[] = {"unzip", "-q", fileName[0], "-d", dir[0], NULL};
    
    child = fork();
    controller = 3;
	if (child==0) {
        printf("3 - Unzip Quote\n");
	    execv("/usr/bin/unzip", argv1);
	}

    pthread_exit(NULL);
}

void* unzipMusic(void *arg)
{   
    while(1){
        if(controller == 2)
            break;
    }
    sleep(2);
    char *argv1[] = {"unzip", "-q", fileName[1], "-d", dir[1], NULL};
    
    child = fork();
    controller = 4;
	if (child==0) {
        printf("4 - Unzip Music\n");
	    execv("/usr/bin/unzip", argv1);
	}
    
    pthread_exit(NULL);
}
```
7. Fungsi `decodeQ()` dan `decode()`

Fungsi `decodeQ()` berfungsi untuk melakukan decode pada masing-masing file pada folder quote dan hasilnya dimasukkan ke dalam quote/quote.txt. Sedangkan Fungsi `decode()` berfungsi untuk melakukan decode pada masing-masing file pada folder music dan hasilnya dimasukkan ke dalam music/music.txt. Fungsi ini akan berjalan apabila nilai variabel controllernya adalah 4 dan seterusnya. 

```c
void* decodeQ1(void *arg)
{
    while(1){
        if(controller == 4)
            break;
    }
    sleep(3);
    controllerQ = 5;
    printf("Q1\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q1.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decode1(void *arg)
{
    while(1){
        if(controller == 4)
            break;
    }
    sleep(2); 
    controllerM = 5;
    printf("M1\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m1.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}
```

8. Fungsi `enter()`

Fungsi ini berfungsi untuk memberikan newline baik pada file quote.txt dan music.txt. 

```c
void* enter2(void *arg)
{
    while(1){
        if(controllerM == 7)
            break;
    }
    sleep(1);
    controllerM = 8;
    printf("EnterM2\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}
```

9. Fungsi `newDirectory`

Fungsi ini berfungsi berfungsi untuk membuat folder hasil di mana nantinya akan berisi file quote.txt, music.txt, dan no.txt. Fungsi ini akan berjalan apabila nilai controllernya adalah 70.

```c
void* newDirectory(void *arg)
{   
    while(1){
        if(controller == 70)
            break;
    }

    mkdir(newFolder, S_IRWXU);
    
    printf("7 - Make New Folder\n");
    controller = 22;

    pthread_exit(NULL);
}
```

10. Fungsi `moveFile()`

Fungsi ini berfungsi untuk memindahkan file music/music.txt dan quote/quote.txt ke folder hasil/. Fungsi ini akan berjalan apabila nilai controllernya adalah 22.

```c
void* moveFile(void *arg){
    while(1){
        if(controller == 22)
            break;
    }

    char *argv1[] = {"mv", "music/music.txt", "hasil/", NULL};
    char *argv2[] = {"mv", "quote/quote.txt", "hasil/", NULL};

    controller = 23;
    child = fork();
    if (child==0) {
	    execv("/usr/bin/mv", argv2);
    }
    child1 = fork();
    if (child1==0){
        printf("8 - Move File Quote.txt & Music.txt\n");
        execv("/usr/bin/mv", argv1);
    }
    
    pthread_exit(NULL);
}
```

11. Fungsi `zipFold()`

Fungsi ni berfungsi untuk melakuan zip pada folder hasil. "-r" artinya akan dilakukan zip seluruh file yang ada pada folder hasil. "-P" artinya zip ditambahkan password. Fungsi ini akan berjalan apabila nilai controllernya adalah 23. Selain itu, pada fungsi ini juga dilakukan penghapusan folder hasil termasuk isi file yang ada di dalamnya.

```c
void* zipFold(void *arg){
    while(1){
        if(controller == 23)
            break;
    }
    sleep(3);

    char *argv1[] = {"zip", "-rP", passZip, "/home/ilham/Documents/modul3/soal1:/hasil.zip", "hasil/", NULL};
    char *argv2[] = {"rm", "-r", "hasil/", NULL};
    printf("9 - Zip Folder Hasil dan hapus folder hasil\n");
    controller = 24;
    child = fork();
    if (child==0) {
        execv("/usr/bin/zip", argv1);
    }
    child1 = fork();
    if (child1==0) {
        sleep(2);
        execv("/usr/bin/rm", argv2);
    }
    
    pthread_exit(NULL);
}
```

12. Fungsi `noTxtAndUnzip()`

Fungsi ini berfungsi untuk membuat file no.txt yang berisi "No" dan unzip pada file hasil.zip. Fungsi ini akan berjalan apabila nilai controllernya adalah 24.

```c
void* noTxtAndUnzip(void *arg){
    while(1){
        if(controller == 24)
            break;
    }
    sleep(3);
    controller = 25;
    printf("10 - Buat No.txt dan unzip hasil.zp\n");
    child = fork();
    if (child==0) {
        execl("/bin/sh", "sh", "-c", "echo \"No\" >> no.txt", (char *) NULL);
    }
    child1 = fork();
    if (child1==0){
        char *argv1[] = {"unzip", "-P", passZip, "hasil.zip", NULL};
        execv("/usr/bin/unzip", argv1);
    }
    
    pthread_exit(NULL);
}
```

13. Fungsi `soal3e()`

Fungsi ini berfungsi untuk memindahkan file no.txt ke folder hasil, menghapus file hasil.zip, dan menzip folder hasil. Fungsi ini akan berjalan apabila variabel controller bernilai 25.

```c
void* soal3e(void *arg){
    while(1){
        if(controller == 25)
            break;
    }
    sleep(2);
   
    printf("11 - Pindah no.txt ke hasil dan zip folder hasil\n");
    child = fork();
    if (child==0) {
        sleep(2);
	    
        execl("/bin/sh", "sh", "-c", "mv no.txt hasil/", (char *) NULL);
    }
    child1 = fork();
    if (child1==0) {
        char *argv1[] = {"rm", "hasil.zip", NULL};
        execv("/usr/bin/rm", argv1);
    }
    child2 = fork();
    if (child2==0) {
        sleep(4);
        char *argv1[] = {"zip", "-rP", passZip, "/home/ilham/Documents/modul3/soal1:/hasil.zip", "hasil/", NULL};
        execv("/usr/bin/zip", argv1);
    }

    pthread_exit(NULL);
}
```

## Screenshot Hasil
![Gambar Hasil No.1](./img/Folder-soal1:.png)
![Gambar Hasil No.1](./img/Hasil-folder-hasil.png)
![Gambar Hasil No.1](./img/Hasil-unzip-music.png)
![Gambar Hasil No.1](./img/Hasil-unzip-quote.png)
![Gambar Hasil No.1](./img/Isi-file-hasil.zip.png)
![Gambar Hasil No.1](./img/Isi-music.txt.png)
![Gambar Hasil No.1](./img/Isi-no.txt.png)
![Gambar Hasil No.1](./img/Isi-quote.txt.png)

## Kendala
- Kesulitan dalam pengendalian thread.

## Soal 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana.

### Soal 2a.
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:

- Username unique (tidak boleh ada user yang memiliki username yang sama)
- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

### Pembahasan 2a.
Pada soal kali ini, kita diminta untuk membuat sebuah program yang terhubung antara client dan server. Di dalamnya kita diminta untuk membuat fitur register dan login. Untuk register terdapat kriteria tertentu. Disini kita buat buat terlebih dahulu file users.txt dengan melakukan deklarasi variable global.
``` c
char *usersTxt = "users.txt";
```

Kemudian memastikan client dan server terhubung dengan
``` c
int main(int argc, char const *argv[])
{
  struct sockaddr_in address;
  int sock = 0, Baca;
  struct sockaddr_in serv_addr;
  char *hello = "Hello from client";
  char buffer[1024] = {0};

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("\n Socket creation error \n");
    return -1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);

  if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
  {
    printf("\nInvalid address/ Address not supported \n");
    return -1;
  }

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("\nGagal Koneksi \n");
    return -1;
  }
```

Kemudian setelah terkoneksi dengan baik maka user dapat melakukan input berupa register atau login.
``` c
while (1)
  {
    char *input;
    printf("Pilih register/login: ");
    scanf("%s", input);
    send(sock, input, strlen(input), 0);
```
Apabila user register maka user harus melakukan input berupa username atau password dengan kriteria yang sudah ditentukan diatas. Dengan source code seperti dibawah.
``` c
if (strcmp(input, "register") == 0)
    {
      memset(buffer, 0, 1024);
      for (int i = 0; i < 2; i++)
      {
        if (i == 0)
        {
          printf("Buat Username: ");
          scanf("%s", input);
          send(sock, input, strlen(input), 0);
        }
        else if (i == 1)
        {
          printf("Buat password: ");
          scanf("%s", input);
          send(sock, input, strlen(input), 0);
        }
      }
      Baca = read(sock, buffer, 1024);

      if (strcmp(buffer, "YES") == 0)
      {
        printf("User baru berhasil ditambahkan \n");
      }
      else if (strcmp(buffer, "NO") == 0)
      {
        printf("User baru gagal ditambahkan \n");
      }
    }
```
Code diatas adalah untuk register, sedangkan untuk login seperti dibawah.
``` c
else if (strcmp(input, "login") == 0)
    {
      memset(buffer, 0, 1024);
      for (int i = 0; i < 2; i++)
      {
        if (i == 0)
        {
          printf("Input username: ");
          scanf("%s", input);
          send(sock, input, strlen(input), 0);
        }
        else if (i == 1)
        {
          printf("Input password: ");
          scanf("%s", input);
          send(sock, input, strlen(input), 0);
        }
      }
```

Untuk logic dari kriteria yang sudah ditentukan seperti username unique dan terdapat alphanumeric seperti dibawah.
```c
int isUserExist(struct UserData user)
{
  FILE *f = fopen(usersTxt, "r");
  if (f == NULL)
  {
    printf("Error opening users.txt file \n");
    exit(1);
  }

  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];

  while (fgets(buffer, MAX_LENGTH, f))
  {
    char *token = strtok(buffer, ":");

    if (strcmp(token, user.id) == 0)
    {
      return 1;
    }
  }

  fclose(f);
  return 0;
}
```
Code diatas untuk mengecek apakah sudah terdapat user yang telah digunakan sebelumnya apabila tidak ada maka program akan mengecek kriteria kedua seperti dibawah.
```c
int isPasswordPass(struct UserData user)
{
  if (strlen(user.password) < 6)
  {
    return 0;
  }

  int i = 0;
  int statusPass = 0;
  while (user.password[i])
  {
    if (isdigit(user.password[i]))
    {
      statusPass = 1;
      break;
    }
    i++;
  }
  if (!statusPass)
  {
    return 0;
  }

  i = statusPass = 0;
  while (user.password[i])
  {
    if (isupper(user.password[i]))
    {
      statusPass = 1;
      break;
    }
    i++;
  }
  if (!statusPass)
  {
    return 0;
  }

  i = 0;
  while (user.password[i])
  {
    if (islower(user.password[i]))
    {
      return 1;
    }
    i++;
  }

  return 0;
}
```
Apabila tidak ada kendala maka sistem akan memasukkan input register ke dalam `users.txt` dengan program seperti dibawah.
``` c
int addUserToUsersTxt(struct UserData user)
{
  int userExist = 2;
  userExist = isUserExist(user);

  int passwordPass = 2;
  passwordPass = isPasswordPass(user);

  if (userExist == 1 || passwordPass == 0)
  {
    return 0;
  }
  else if (userExist == 0 && passwordPass == 1)
  {
    FILE *f = fopen(usersTxt, "a");
    if (f == NULL)
    {
      printf("Error opening users.txt file \n");
      exit(1);
    }

    fprintf(f, "%s:%s\n", user.id, user.password);
    fclose(f);

    return 1;
  }
}
```
Kemudian user dapat login kembali dengan user yang sudah didaftarkan dengan menginputkan command login dan program akan mencari data user dalam `users.txt`
``` c
nt checkLogin(struct UserData user)
{
  FILE *f = fopen(usersTxt, "r");
  if (f == NULL)
  {
    printf("Error opening users.txt file \n");
    exit(1);
  }
```

### Soal 2b.
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

### Pembahasan 2b.
Dalam soal ini kita diminta untuk membuat sebuah database yang berisikan judul problem dan file yang diinputkan. Dengan code seperti dibawah.
``` c
void *createTsv()
{
  FILE *f = fopen("problem.tsv", "a");
  fclose(f);
}
```
### Soal 2c.
Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
- Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
- Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
- Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
- Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

### Pembahasan 2c.
Setelah pada soal sebelumnya kita membuat database. Pada soal ini kita diminta untuk membuat folder berisikan file-file yang akan dibuat. Pertama kita buat foldernya terlebih dahulu.
``` c
void createProblemFolder(char *problemName)
{
  if (mkdir(problemName, 0777) == -1)
  {
    printf("Error when create folder %s \n", problemName);
    exit(0);
  }
```

Kemudian akan dilakukan pengecekan di dalam folder apakah ada file yang dibuat sebelumnya.
``` c
int isProblemExist(char *problemName)
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir(".");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strcmp(ep->d_name, "problem.tsv") != 0 && strcmp(ep->d_name, "server") != 0 && strcmp(ep->d_name, "server.c") != 0 && strcmp(ep->d_name, "users.txt") != 0)
      {
        if (strcmp(problemName, ep->d_name) == 0)
        {
          return 1;
        }
      }
    }
    return 0;
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Setelah membuat folder dan dilakukan pengecekan maka akan dibuat secara otomatis file `input.txt` , `description.txt` , `output.txt`. Seperti dibawah.
``` c
char desc[100], in[100], out[100];
  strcpy(desc, problemName);
  strcpy(in, problemName);
  strcpy(out, problemName);
  strcat(desc, "/description.txt");
  strcat(in, "/input.txt");
  strcat(out, "/output.txt");

  FILE *f1 = fopen(desc, "a");
  fclose(f1);
  FILE *f2 = fopen(in, "a");
  fclose(f2);
  FILE *f3 = fopen(out, "a");
  fclose(f3);
```
### Soal 2d.
Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut).

### Pembahasan 2d.
Pada soal ini kita diminta untuk membuat command `see` sebagai perintah untuk menampilkan judul problem yang kita buat sebelumnya. Source code seperti dibawah.
``` c
void showListProblem()
{
  FILE *f = fopen("problem.tsv", "r");
  if (f == NULL)
  {
    printf("Error opening problem.tsv file \n");
    exit(1);
  }

  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];

  while (fgets(buffer, MAX_LENGTH, f))
  {
    char *token = strtok(buffer, ":");
    printf("%s by ", token);
    token = strtok(NULL, ":");
    printf("%s\n", token);
  }
  printf("\n");

  fclose(f);
}
```
## Screenshot Hasil
![Gambar Hasil No.1](./img/Pict_1.png)
![Gambar Hasil No.1](./img/Pict_2.png)
![Gambar Hasil No.1](./img/Pict_3.png)
![Gambar Hasil No.1](./img/Pict_4.png)
![Gambar Hasil No.1](./img/Pict_5.png)

## Kendala
- Pada soal ini kami terkendala dalam memahami logic permintaan tiap soal.
- SUmber referensi belajar kami kurang.
- Sering terjadi error pada saat menghubungkan client dan server.

## Soal 3
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
a.
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif
b.
Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.
c.
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.
d.
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.
e.
Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command

## Cara pengerjaan
## 3A. Extract file
Untuk melakukan extract digunakan fungsi `popen()` dengan isi path ke file .zip-nya dan r untuk membaca file. Kemudian digunakan fungsi fread pada variabel chars_read untuk melakukan print proses ekstraksi file di dalam arsip ke terminal.

```c
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```

[Kembali ke Daftar Isi](#daftar-isi)
