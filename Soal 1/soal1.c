#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <sys/stat.h>
 
pthread_t tid[2];
pthread_t tidUnzip[2]; // untuk unzip quote.zip dan music.zip
pthread_t textDecode[2];
pthread_t tidQ[18];
pthread_t tidM[18];
pthread_t newFold;
pthread_t move;
pthread_t tidZip;
pthread_t txt;
pthread_t last;
pid_t child;
pid_t child1, child2;

// {quote, music}
char *url[] = {"https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"};
char *fileName[] = {"/home/ilham/Documents/modul3/soal1:/quote.zip", "/home/ilham/Documents/modul3/soal1:/music.zip"};
char *dir[] = {"/home/ilham/Documents/modul3/soal1:/quote", "/home/ilham/Documents/modul3/soal1:/music"};
char *fileQuote[] = {"/home/ilham/Documents/modul3/soal1:/quote/q1.txt", "/home/ilham/Documents/modul3/soal1:/quote/q2.txt", "/home/ilham/Documents/modul3/soal1:/quote/q3.txt", "/home/ilham/Documents/modul3/soal1:/quote/q4.txt", "/home/ilham/Documents/modul3/soal1:/quote/q5.txt", "/home/ilham/Documents/modul3/soal1:/quote/q6.txt", "/home/ilham/Documents/modul3/soal1:/quote/q7.txt", "/home/ilham/Documents/modul3/soal1:/quote/q8.txt", "/home/ilham/Documents/modul3/soal1:/quote/q9.txt"};
char *fileMusic[] = {"/home/ilham/Documents/modul3/soal1:/music/m1.txt", "/home/ilham/Documents/modul3/soal1:/music/m2.txt", "/home/ilham/Documents/modul3/soal1:/music/m3.txt", "/home/ilham/Documents/modul3/soal1:/music/m4.txt", "/home/ilham/Documents/modul3/soal1:/music/m5.txt", "/home/ilham/Documents/modul3/soal1:/music/m6.txt", "/home/ilham/Documents/modul3/soal1:/music/m7.txt", "/home/ilham/Documents/modul3/soal1:/music/m8.txt", "/home/ilham/Documents/modul3/soal1:/music/m9.txt"};
char *decode[] = {"/home/ilham/Documents/modul3/soal1:/quote/quote.txt", "/home/ilham/Documents/modul3/soal1:/music/music.txt"};
char *newFolder = "hasil";
char *passZip = "mihinomenestilham";

int controller = 0;
int controllerQ = 0;
int controllerM = 0;
int nomor;
 
void* downloadQuote(void *arg)
{   
    char *argv1[] = {"wget", "-q", url[0], "-O", fileName[0], NULL};
    controller = 1;
    child = fork();
	if (child==0) {
        
        printf("1 - Download Quote\n");
	    execv("/usr/bin/wget", argv1);
	}

    pthread_exit(NULL);
}

void* downloadMusic(void *arg)
{   
    char *argv1[] = {"wget", "-q", url[1], "-O", fileName[1], NULL};
    
    child = fork();
    controller = 2;
	if (child==0) {
        printf("2 - Download Music\n");
	    execv("/usr/bin/wget", argv1);
	}

    pthread_exit(NULL);
}

void* unzipQuote(void *arg)
{   
    while(1){
        if(controller == 2)
            break;
    }
    sleep(2);
    char *argv1[] = {"unzip", "-q", fileName[0], "-d", dir[0], NULL};
    
    child = fork();
    controller = 3;
	if (child==0) {
        printf("3 - Unzip Quote\n");
	    execv("/usr/bin/unzip", argv1);
	}

    pthread_exit(NULL);
}

void* unzipMusic(void *arg)
{   
    while(1){
        if(controller == 2)
            break;
    }
    sleep(2);
    char *argv1[] = {"unzip", "-q", fileName[1], "-d", dir[1], NULL};
    
    child = fork();
    controller = 4;
	if (child==0) {
        printf("4 - Unzip Music\n");
	    execv("/usr/bin/unzip", argv1);
	}
    
    pthread_exit(NULL);
}

void* decodeQ1(void *arg)
{
    while(1){
        if(controller == 4)
            break;
    }
    sleep(3);
    controllerQ = 5;
    printf("Q1\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q1.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enterQ1(void *arg)
{
    while(1){
        if(controllerQ == 5)
            break;
    }
    sleep(1);
    controllerQ = 6;
    printf("EnterQ1\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}
	
void* decodeQ2(void *arg)
{
    while(1){
        if(controllerQ == 6)
            break;
    }
    sleep(1);
    controllerQ = 7;
    printf("Q2\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q2.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enterQ2(void *arg)
{
    while(1){
        if(controllerQ == 7)
            break;
    }
    sleep(1);
    controllerQ = 8;
    printf("EnterQ2\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}
	
void* decodeQ3(void *arg)
{
    while(1){
        if(controllerQ == 8)
            break;
    }
    sleep(1);
    controllerQ = 9;
    printf("Q3\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q3.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enterQ3(void *arg)
{
    while(1){
        if(controllerQ == 9)
            break;
    }
    sleep(1);
    controllerQ = 10;
    printf("EnterQ3\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}
	
void* decodeQ4(void *arg)
{
    while(1){
        if(controllerQ == 10)
            break;
    }
    sleep(1);
    controllerQ = 11;
    printf("Q4\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q4.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enterQ4(void *arg)
{
    while(1){
        if(controllerQ == 11)
            break;
    }
    sleep(1);
    controllerQ = 12;
    printf("EnterQ4\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decodeQ5(void *arg)
{
    while(1){
        if(controllerQ == 12)
            break;
    }
    sleep(1);
    controllerQ = 13;
    printf("Q5\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q5.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enterQ5(void *arg)
{
    while(1){
        if(controllerQ == 13)
            break;
    }
    sleep(1);
    controllerQ = 14;
    printf("EnterQ5\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decodeQ6(void *arg)
{
    while(1){
        if(controllerQ == 14)
            break;
    }
    sleep(1);
    controllerQ = 15;
    printf("Q6\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q6.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enterQ6(void *arg)
{
    while(1){
        if(controllerQ == 15)
            break;
    }
    sleep(1);
    controllerQ = 16;
    printf("EnterQ6\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decodeQ7(void *arg)
{
    while(1){
        if(controllerQ == 16)
            break;
    }
    sleep(1);
    controllerQ = 17;
    printf("Q7\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q7.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enterQ7(void *arg)
{
    while(1){
        if(controllerQ == 17)
            break;
    }
    sleep(1);
    controllerQ = 18;
    printf("EnterQ7\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decodeQ8(void *arg)
{
    while(1){
        if(controllerQ == 18)
            break;
    }
    sleep(1);
    controllerQ = 19;
    printf("Q8\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q8.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enterQ8(void *arg)
{
    while(1){
        if(controllerQ == 19)
            break;
    }
    sleep(1);
    controllerQ = 20;
    printf("EnterQ8\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decodeQ9(void *arg)
{
    while(1){
        if(controllerQ == 20)
            break;
    }
    sleep(1);
    controllerQ = 21;
    printf("Q9\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d quote/q9.txt >> quote/quote.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

// decode music
void* decode1(void *arg)
{
    while(1){
        if(controller == 4)
            break;
    }
    sleep(2); //prob
    controllerM = 5;
    printf("M1\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m1.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enter1(void *arg)
{
    while(1){
        if(controllerM == 5)
            break;
    }
    sleep(1);
    controllerM = 6;
    printf("EnterM1\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}
	
void* decode2(void *arg)
{
    while(1){
        if(controllerM == 6)
            break;
    }
    sleep(1);
    controllerM = 7;
    printf("M2\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m2.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enter2(void *arg)
{
    while(1){
        if(controllerM == 7)
            break;
    }
    sleep(1);
    controllerM = 8;
    printf("EnterM2\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}
	
void* decode3(void *arg)
{
    while(1){
        if(controllerM == 8)
            break;
    }
    sleep(1);
    controllerM = 9;
    printf("M3\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m3.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enter3(void *arg)
{
    while(1){
        if(controllerM == 9)
            break;
    }
    sleep(1);
    controllerM = 10;
    printf("EnterM3\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}
	
void* decode4(void *arg)
{
    while(1){
        if(controllerM == 10)
            break;
    }
    sleep(1);
    controllerM = 11;
    printf("M4\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m4.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enter4(void *arg)
{
    while(1){
        if(controllerM == 11)
            break;
    }
    sleep(1);
    controllerM = 12;
    printf("EnterM4\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decode5(void *arg)
{
    while(1){
        if(controllerM == 12)
            break;
    }
    sleep(1);
    controllerM = 13;
    printf("M5\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m5.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enter5(void *arg)
{
    while(1){
        if(controllerM == 13)
            break;
    }
    sleep(1);
    controllerM = 14;
    printf("EnterM5\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decode6(void *arg)
{
    while(1){
        if(controllerM == 14)
            break;
    }
    sleep(1);
    controllerM = 15;
    printf("M6\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m6.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enter6(void *arg)
{
    while(1){
        if(controllerM == 15)
            break;
    }
    sleep(1);
    controllerM = 16;
    printf("EnterM6\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decode7(void *arg)
{
    while(1){
        if(controllerM == 16)
            break;
    }
    sleep(1);
    controllerM = 17;
    printf("M7\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m7.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enter7(void *arg)
{
    while(1){
        if(controllerM == 17)
            break;
    }
    sleep(1);
    controllerM = 18;
    printf("EnterM7\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decode8(void *arg)
{
    while(1){
        if(controllerM == 18)
            break;
    }
    sleep(1);
    controllerM = 19;
    printf("M8\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m8.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* enter8(void *arg)
{
    while(1){
        if(controllerM == 19)
            break;
    }
    sleep(1);
    controllerM = 20;
    printf("EnterM8\n");
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "echo '' >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* decode9(void *arg)
{
    while(1){
        if(controllerQ == 21)
            break;
    }
    sleep(1);
    controller = 70;
    printf("M9\n"); //-------
	child = fork();
	if (child==0) {
		execl("/bin/sh", "sh", "-c", "base64 -d music/m9.txt >> music/music.txt", (char *) NULL);
	}
    pthread_exit(NULL);
}

void* newDirectory(void *arg)
{   
    while(1){
        if(controller == 70)
            break;
    }
    // sleep(1);

    mkdir(newFolder, S_IRWXU);
    
    printf("7 - Make New Folder\n");
    controller = 22;

    pthread_exit(NULL);
}

void* moveFile(void *arg){
    while(1){
        if(controller == 22)
            break;
    }

    char *argv1[] = {"mv", "music/music.txt", "hasil/", NULL};
    char *argv2[] = {"mv", "quote/quote.txt", "hasil/", NULL};

    controller = 23;
    child = fork();
    if (child==0) {
	    execv("/usr/bin/mv", argv2);
    }
    child1 = fork();
    if (child1==0){
        printf("8 - Move File Quote.txt & Music.txt\n");
        execv("/usr/bin/mv", argv1);
    }
    
    pthread_exit(NULL);
}

void* zipFold(void *arg){
    while(1){
        if(controller == 23)
            break;
    }
    sleep(3);

    char *argv1[] = {"zip", "-rP", passZip, "/home/ilham/Documents/modul3/soal1:/hasil.zip", "hasil/", NULL};
    char *argv2[] = {"rm", "-r", "hasil/", NULL};
    printf("9 - Zip Folder Hasil dan hapus folder hasil\n");
    controller = 24;
    child = fork();
    if (child==0) {
        execv("/usr/bin/zip", argv1);
    }
    child1 = fork();
    if (child1==0) {
        sleep(2);
        execv("/usr/bin/rm", argv2);
    }
    
    pthread_exit(NULL);
}
///
void* noTxtAndUnzip(void *arg){
    while(1){
        if(controller == 24)
            break;
    }
    sleep(3);
    controller = 25;
    printf("10 - Buat No.txt dan unzip hasil.zp\n");
    child = fork();
    if (child==0) {
        //  execl("/bin/sh", "sh", "-c", "rm -f /home/sharira/modul2/darat/*bird*", (char *) NULL);
        execl("/bin/sh", "sh", "-c", "echo \"No\" >> no.txt", (char *) NULL);
    }
    child1 = fork();
    if (child1==0){
        char *argv1[] = {"unzip", "-P", passZip, "hasil.zip", NULL};
        execv("/usr/bin/unzip", argv1);
    }
    
    pthread_exit(NULL);
}

void* soal3e(void *arg){
    while(1){
        if(controller == 25)
            break;
    }
    sleep(2);
    // controller = 8;
    printf("11 - Pindah no.txt ke hasil dan zip folder hasil\n");
    child = fork();
    if (child==0) {
        sleep(2);
	    // execl("/bin/sh", "sh", "-c", "mv no.txt hasil/ | zip -P mihinomenestilham hasil.zip hasil/", (char *) NULL);
        execl("/bin/sh", "sh", "-c", "mv no.txt hasil/", (char *) NULL);
    }
    child1 = fork();
    if (child1==0) {
        char *argv1[] = {"rm", "hasil.zip", NULL};
        execv("/usr/bin/rm", argv1);
    }
    child2 = fork();
    if (child2==0) {
        sleep(4);
        char *argv1[] = {"zip", "-rP", passZip, "/home/ilham/Documents/modul3/soal1:/hasil.zip", "hasil/", NULL};
        execv("/usr/bin/zip", argv1);
    }

    pthread_exit(NULL);
}

 
int main(void)
{
    pthread_create(&(tid[0]),NULL,&downloadQuote,NULL); //membuat thread download quote
    pthread_create(&(tid[1]),NULL,&downloadMusic,NULL); //membuat thread download music
    pthread_create(&(tidUnzip[0]),NULL,&unzipQuote,NULL); //membuat thread unzip quote
    pthread_create(&(tidUnzip[1]),NULL,&unzipMusic,NULL); //membuat thread unzip music

    pthread_create(&(tidQ[0]),NULL,&decodeQ1,NULL); //qeqbuat thread
    pthread_create(&(tidQ[1]),NULL,&enterQ1,NULL); //qeqbuat thread
    pthread_create(&(tidQ[2]),NULL,&decodeQ2,NULL); //qeqbuat thread
    pthread_create(&(tidQ[3]),NULL,&enterQ2,NULL); //qeqbuat thread
    pthread_create(&(tidQ[4]),NULL,&decodeQ3,NULL); //qeqbuat thread
    pthread_create(&(tidQ[5]),NULL,&enterQ3,NULL); //qeqbuat thread
    pthread_create(&(tidQ[6]),NULL,&decodeQ4,NULL); //qeqbuat thread
    pthread_create(&(tidQ[7]),NULL,&enterQ4,NULL); //qeqbuat thread
    pthread_create(&(tidQ[8]),NULL,&decodeQ5,NULL); //qeqbuat thread
    pthread_create(&(tidQ[9]),NULL,&enterQ5,NULL); //qeqbuat thread
    pthread_create(&(tidQ[10]),NULL,&decodeQ6,NULL); //qeqbuat thread
    pthread_create(&(tidQ[11]),NULL,&enterQ6,NULL); //qeqbuat thread
    pthread_create(&(tidQ[12]),NULL,&decodeQ7,NULL); //qeqbuat thread
    pthread_create(&(tidQ[13]),NULL,&enterQ7,NULL); //qeqbuat thread
    pthread_create(&(tidQ[14]),NULL,&decodeQ8,NULL); //qeqbuat thread
    pthread_create(&(tidQ[15]),NULL,&enterQ8,NULL); //qeqbuat thread
    pthread_create(&(tidQ[16]),NULL,&decodeQ9,NULL); //qeqbuat thread

    pthread_create(&(tidM[0]),NULL,&decode1,NULL); //membuat thread
    pthread_create(&(tidM[1]),NULL,&enter1,NULL); //membuat thread
    pthread_create(&(tidM[2]),NULL,&decode2,NULL); //membuat thread
    pthread_create(&(tidM[3]),NULL,&enter2,NULL); //membuat thread
    pthread_create(&(tidM[4]),NULL,&decode3,NULL); //membuat thread
    pthread_create(&(tidM[5]),NULL,&enter3,NULL); //membuat thread
    pthread_create(&(tidM[6]),NULL,&decode4,NULL); //membuat thread
    pthread_create(&(tidM[7]),NULL,&enter4,NULL); //membuat thread
    pthread_create(&(tidM[8]),NULL,&decode5,NULL); //membuat thread
    pthread_create(&(tidM[9]),NULL,&enter5,NULL); //membuat thread
    pthread_create(&(tidM[10]),NULL,&decode6,NULL); //membuat thread
    pthread_create(&(tidM[11]),NULL,&enter6,NULL); //membuat thread
    pthread_create(&(tidM[12]),NULL,&decode7,NULL); //membuat thread
    pthread_create(&(tidM[13]),NULL,&enter7,NULL); //membuat thread
    pthread_create(&(tidM[14]),NULL,&decode8,NULL); //membuat thread
    pthread_create(&(tidM[15]),NULL,&enter8,NULL); //membuat thread
    pthread_create(&(tidM[16]),NULL,&decode9,NULL); //membuat thread


    pthread_create(&(newFold),NULL,&newDirectory, NULL); //membuat thread untuk membuat folder hasil
    pthread_create(&(move),NULL,&moveFile, NULL); //membuat thread untuk memindahkan file music.txt dan quote.txt ke folder hasil
    pthread_create(&(tidZip),NULL,&zipFold, NULL); //membuat thread untuk zip folder dgn password
    pthread_create(&(txt),NULL,&noTxtAndUnzip, NULL); //membuat thread untuk tambah no.txt dan unzip hasil.zip
    pthread_create(&(last),NULL,&soal3e, NULL); //membuat thread untuk pindahkan file no.txt ke folder hasil dan zip folder hasil

 
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tidUnzip[0], NULL);
    pthread_join(tidUnzip[1], NULL);
    
    pthread_join(tidQ[0],NULL);
	pthread_join(tidQ[1],NULL);
    pthread_join(tidQ[2],NULL);
    pthread_join(tidQ[3],NULL);
    pthread_join(tidQ[4],NULL);
    pthread_join(tidQ[5],NULL);
    pthread_join(tidQ[6],NULL);
    pthread_join(tidQ[6],NULL);
    pthread_join(tidQ[7],NULL);
    pthread_join(tidQ[8],NULL);
    pthread_join(tidQ[9],NULL);
    pthread_join(tidQ[10],NULL);
    pthread_join(tidQ[11],NULL);
    pthread_join(tidQ[12],NULL);
    pthread_join(tidQ[13],NULL);
    pthread_join(tidQ[14],NULL);
    pthread_join(tidQ[15],NULL);
    pthread_join(tidQ[16],NULL);

    pthread_join(tidM[0],NULL);
	pthread_join(tidM[1],NULL);
    pthread_join(tidM[2],NULL);
    pthread_join(tidM[3],NULL);
    pthread_join(tidM[4],NULL);
    pthread_join(tidM[5],NULL);
    pthread_join(tidM[6],NULL);
    pthread_join(tidM[6],NULL);
    pthread_join(tidM[7],NULL);
    pthread_join(tidM[8],NULL);
    pthread_join(tidM[9],NULL);
    pthread_join(tidM[10],NULL);
    pthread_join(tidM[11],NULL);
    pthread_join(tidM[12],NULL);
    pthread_join(tidM[13],NULL);
    pthread_join(tidM[14],NULL);
    pthread_join(tidM[15],NULL);
    pthread_join(tidM[16],NULL);

    pthread_join(newFold, NULL);
    pthread_join(move, NULL);
    pthread_join(tidZip, NULL);
    pthread_join(txt, NULL);
    pthread_join(last, NULL);

 
    return 0;
}
